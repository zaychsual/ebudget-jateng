<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Blog extends Migration
{
	public function up()
	{
		$this->forge->addField([
            'id' => [
            	'type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'auto_increment' => TRUE
            ],
            'post_title' => [
            	'type'      => 'VARCHAR', 'constraint'=> 255
            ],
            'description' => [
            	'type'      => 'TEXT'
            ],
            'status' => [
            	'type'      => 'BOOLEAN'
            ],
            'created_by' => [
            	'type'      => 'VARCHAR', 'constraint'=> 255, 'null' => TRUE
            ],
            'updated_by' => [
            	'type'      => 'VARCHAR', 'constraint'=> 255, 'null' => TRUE
            ],
            'created_at' => [
            	'type'      => 'TIMESTAMP', 'null' => TRUE
            ],
            'updated_at' => [
            	'type'      => 'TIMESTAMP', 'null' => TRUE
            ]
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('blog');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('blog');
	}
}
