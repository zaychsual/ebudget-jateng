<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class OauthJwt extends Migration
{
	public function up()
	{
		$this->forge->addField([
            'client_id' => [
            	'type' => 'VARCHAR', 'constraint' => 80, 'null' => FALSE
            ],
            'subject' => [
            	'type' => 'VARCHAR', 'constraint' => 80, 'null' => TRUE
            ],
            'public_key' => [
            	'type' => 'VARCHAR', 'constraint' => 80, 'null' => FALSE
            ]
        ]);
        $this->forge->addKey('client_id', TRUE);
        $this->forge->createTable('oauth_jwt');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('oauth_jwt');
	}
}
