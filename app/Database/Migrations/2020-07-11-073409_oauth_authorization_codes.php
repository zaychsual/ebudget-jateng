<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class OauthAuthorizationCodes extends Migration
{
	public function up()
	{
		$this->forge->addField([
            'authorization_code' => [
            	'type'		=> 'VARCHAR', 'constraint'=> 40, 'null' => FALSE
            ],
            'client_id' => [
				'type'      => 'VARCHAR', 'constraint'=> 80, 'null' => FALSE
            ],
            'user_id' => [
            	'type'      => 'VARCHAR', 'constraint'=> 80, 'null' => TRUE
            ],
            'redirect_uri' => [
            	'type'      => 'VARCHAR', 'constraint'=> 2000, 'null' => TRUE
            ],
            'expires' => [
            	'type'      => 'TIMESTAMP', 'null' => FALSE
            ],
            'scope' => [
            	'type'      => 'VARCHAR', 'constraint'=> 4000, 'null' => TRUE
            ],
            'id_token' => [
            	'type'      => 'VARCHAR', 'constraint'=> 1000, 'null' => TRUE
            ]
        ]);
        $this->forge->addKey('authorization_code', TRUE);
        $this->forge->createTable('oauth_authorization_codes');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('oauth_authorization_codes');
	}
}
