<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddLevelUser extends Migration
{
	public function up()
	{
		$fields = ['level_user' => 
			['type' => 'INT', 'constraint' => 2, 'null' => TRUE]
		];

		$this->forge->addColumn('oauth_users', $fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropColumn('oauth_users', 'level_user');
	}
}
