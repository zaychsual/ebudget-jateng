<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class OauthUsers extends Migration
{
	public function up()
	{
		$this->forge->addField([
            'id' => [
            	'type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'auto_increment' => TRUE
            ],
            'username' => [
            	'type' => 'VARCHAR', 'constraint' => 40, 'null' => FALSE
            ],
            'password' => [
            	'type' => 'VARCHAR', 'constraint' => 80, 'null' => FALSE
            ],
            'first_name' => [
            	'type' => 'VARCHAR', 'constraint' => 80, 'null' => TRUE
            ],
            'last_name' => [
            	'type' => 'VARCHAR', 'constraint' => 80, 'null' => FALSE
            ],            
            'email' => [
            	'type' => 'VARCHAR', 'constraint' => 80, 'null' => FALSE,
            ],
            'email_verified' => [
            	'type' => 'BOOLEAN', 'null' => TRUE
            ],
            'scope' => [
            	'type' => 'VARCHAR', 'constraint' => 4000, 'null' => TRUE
            ],
            'created_by' => [
                'type' => 'VARCHAR', 'constraint' => 80, 'null' => TRUE
            ],
            'updated_by' => [
                'type' => 'VARCHAR', 'constraint' => 80, 'null' => TRUE
            ],
            'created_at' => [
                'type' => 'TIMESTAMP', 'null' => TRUE
            ],
            'updated_at' => [
                'type' => 'TIMESTAMP', 'null' => TRUE
            ],
            'status' => [
                'type' => 'BOOLEAN', 'null' => TRUE
            ],
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('oauth_users');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('oauth_users');
	}
}
