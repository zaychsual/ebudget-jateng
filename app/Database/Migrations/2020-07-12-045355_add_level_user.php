<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddLevelUser extends Migration
{
	public function up()
	{
		$fields = ['level_user' => 
			['type' => 'INT', 'constraint' => 2, 'null' => TRUE]
		];

		$this->forge->addColumn('users', $fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropColumn('users', 'level_user');
	}
}
