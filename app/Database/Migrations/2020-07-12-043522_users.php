<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Users extends Migration
{
	public function up()
	{
		$this->forge->addField([
            'id' => [
            	'type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'auto_increment' => TRUE
            ],
            'nik' => [
            	'type' => 'VARCHAR', 'constraint' => 40, 'null' => FALSE
            ],
            'password' => [
            	'type' => 'VARCHAR', 'constraint' => 80, 'null' => FALSE
            ],
            'firstname' => [
            	'type' => 'VARCHAR', 'constraint' => 80, 'null' => TRUE
            ],
            'lastname' => [
            	'type' => 'VARCHAR', 'constraint' => 80, 'null' => FALSE
            ],            
            'email' => [
            	'type' => 'VARCHAR', 'constraint' => 80, 'null' => FALSE,
            ],
            'email_verified' => [
            	'type' => 'BOOLEAN', 'null' => TRUE
            ],
            'scope' => [
            	'type' => 'VARCHAR', 'constraint' => 4000, 'null' => TRUE
            ],
            'created_by' => [
                'type' => 'VARCHAR', 'constraint' => 80, 'null' => TRUE
            ],
            'updated_by' => [
                'type' => 'VARCHAR', 'constraint' => 80, 'null' => TRUE
            ],
            'created_at' => [
                'type' => 'TIMESTAMP', 'null' => TRUE
            ],
            'updated_at' => [
                'type' => 'TIMESTAMP', 'null' => TRUE
            ],
            'status' => [
                'type' => 'BOOLEAN', 'null' => TRUE
            ],
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('users');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('users');
	}
}
