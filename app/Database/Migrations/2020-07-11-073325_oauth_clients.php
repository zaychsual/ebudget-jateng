<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class OauthClients extends Migration
{
	public function up()
	{
		$this->forge->addField([
            'client_id' => [
            	'type'		=> 'VARCHAR', 'constraint'=> 80
            ],
            'client_secret' => [
            	'type'      => 'VARCHAR', 'constraint'=> 80
            ],
            'redirect_uri' => [
            	'type'      => 'VARCHAR', 'constraint'=> 2000, 'null' => TRUE
            ],
            'grant_types' => [
            	'type'      => 'VARCHAR', 'constraint'=> 80, 'null' => TRUE
            ],
            'scope' => [
            	'type'      => 'VARCHAR', 'constraint'=> 4000, 'null' => TRUE
            ],
            'user_id' => [
            	'type'      => 'VARCHAR', 'constraint'=> 80, 'null' => TRUE
            ]
        ]);
        $this->forge->addKey('client_id', TRUE);
        $this->forge->createTable('oauth_clients');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('oauth_clients');
	}
}
