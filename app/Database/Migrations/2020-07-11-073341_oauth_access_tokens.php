<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class OauthAccessTokens extends Migration
{
	public function up()
	{
		$this->forge->addField([
            'access_token' => [
            	'type'		=> 'VARCHAR', 'constraint'=> 40, 'null' => FALSE,
            ],
            'client_id' => [
            	'type'      => 'VARCHAR', 'constraint'=> 80, 'null' => FALSE,
            ],
            'user_id' => [
            	'type'      => 'VARCHAR', 'constraint'=> 80, 'null' => TRUE
            ],
            'expires' => [
            	'type'      => 'TIMESTAMP', 'null' => FALSE
            ],
            'scope' => [
            	'type'      => 'VARCHAR', 'constraint'=> 4000, 'null' => TRUE
            ]
        ]);
        $this->forge->addKey('access_token', TRUE);
        $this->forge->createTable('oauth_access_tokens');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('oauth_access_tokens');
	}
}
