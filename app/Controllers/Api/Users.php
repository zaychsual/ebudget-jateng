<?php namespace App\Controllers\Api;

use App\Controllers\BaseController;
use \App\Libraries\Oauth;
use \OAuth2\Request;
use \CodeIgniter\API\ResponseTrait;
use App\Models\M_User;

class Users extends BaseController
{
	use ResponseTrait;
	protected $format = 'json';

	public function __construct() {
 
		$this->model = new M_User;
        
	}
	
	public function index()
	{
		$data = $this->model->findAll();

		return $this->respond($data);
	}

	public function login()
	{
		$oauth = new OAuth();
		$request = new Request();

		$respond = $oauth->server->handleTokenRequest($request->createFromGlobals());
		$code = $respond->getStatusCode();
		$body = $respond->getResponseBody();

		return $this->respond(json_decode($body), $code);
	} 	

	public function register()
	{
		helper('form');
		$data = [];
		$post = $this->request->getVar();

		if($this->request->getMethod() != 'post')
			return $this->fail('Only Post Request is Allowed');
		
		$rules = [
			'nik' => 'required|min_length[3]|max_length[20]|is_unique[users.nik]',
			'firstname' => 'required|min_length[3]|max_length[20]',
			'email' => 'required|valid_email|is_unique[users.email]',
			'password' => 'required|min_length[8]',
			'password_confirm' => 'matches[password]'
		];

		if(!isset($post['lastname'])) {
			$post['lastname'] = '';
		}

		if(!$this->validate($rules)) {
			return $this->fail($this->validator->getErrors());
		} else {
			$data = [
				'nik' => $post['nik'],
				'password' => $post['password'],
				'firstname' => $post['firstname'],
				'lastname' => $post['lastname'],
				'email' => $post['email'],
				'scope' => 'app',
				'created_at' => date('Y-m-d H:i:s')
			];

			$user_id = $this->model->insert($data);
			$data['id'] = $user_id;
			unset($data['password']);

			return $this->respondCreated($data);
		}
	}

	public function show($id = null)
	{
		$data = $this->model->find($id);

		if($data) {
			return $this->respond($data);
		} else {
			return $this->failNotFound('Item Not Found');
		}
	}

	public function update($id = null)
	{
		helper(['form']);
		$input = $this->request->getRawInput();

		if($this->request->getMethod() != 'put')
			return $this->fail('Only Put Request is Allowed');
		
		$rules = [
			'nik' => 'required',
			'firstname' => 'required|min_length[3]|max_length[20]',
			'email' => 'required|valid_email|is_unique[users.email]',
			'password' => 'required|min_length[8]',
			'password_confirm' => 'matches[password]'
		];
		
		/* $nik = $input['nik'];		
		$email = $input['email'];
		$db = db_connect();
        $builder = $db->table('users');
        $where = ['nik' => $nik];
        $where = ['id !=' => $id];
        $builder->where($where);
		$checkNik = $builder->get()->getRowArray();
		
		if($checkNik != '') {
			return $this->fail('NIK is already registered find another one');
		}

		$builder = $db->table('users');
        $where = ['email' => $email];
        $where = ['id !=' => $id];
		$builder->where($where);
		$checkMail = $builder->get()->getRowArray();
		if(isset($checkMail)) {
			return $this->fail('email is already registered find another one');
		} */

		if(!isset($post['lastname'])) {
			$post['lastname'] = '';
		}

		if(!$this->validate($rules)) {
			return $this->fail($this->validator->getErrors());
		} else {

			$data = [
				'id' => $id,
				'nik' => $input['nik'],
				'firstname' => $input['firstname'],
				'lastname' => $input['lastname'],
				'email' => $input['email'],
				'scope' => 'app',
				'password' => $input['password'],
				'updated_at' => date('Y-m-d H:i:s')
			];
			$this->model->save($data);
			unset($data['password']);

			return $this->respond($data);
		}
	}

	public function delete($id = null)
	{
		$data = $this->model->find($id);
		
		if($data) {
			$this->model->delete($id);
			return $this->respondDeleted($data);
		} else {
			return $this->failNotFound('Item Not Found');
		}
	}
}
