<?php namespace App\Controllers\Api;

use App\Controllers\BaseController;
use \CodeIgniter\API\ResponseTrait;
use App\Models\M_user;
use CodeIgniter\RESTful\ResourceController;
use Config\Services;
use Firebase\JWT\JWT;
// helper('form');

class Auth extends ResourceController
{

    use ResponseTrait;
    protected $format = 'json';

    public function __construct()
    {
        $this->model = new M_user;
    }

	public function login()
	{
        helper('form');
        $data = [];
        $post = $this->request->getVar();
        
        if($this->request->getMethod() != 'post')
            return $this->fail('Only Post Request is Allowed');
        
        $rules = [
            'nik' => 'required',
            'password' => 'required|validateUser[nik,password]'
        ];

        $errors = [
            'password' => [
                'validateUser' => 'Email or Password Dont Match'
            ]
        ];

		// add code to fetch through db and check they are valid
		// sending no email and password also works here because both are empty
        if(!$this->validate($rules)) {
            return $this->fail($errors);
        } else {
            $key = Services::getSecretKey();
            $issuedat_claim = time(); // issued at
			$notbefore_claim = $issuedat_claim + 10; //not before in seconds
            $expire_claim = $issuedat_claim + 86400; // expire time in seconds
            $user = $this->model->where('nik', $this->request->getVar('nik'))
					->first();
			$payload = [
				'aud' => 'http://example.com',
				"iat" => $issuedat_claim,
                "nbf" => $notbefore_claim,
                "exp" => $expire_claim,
                "data" => array(
					"id" => $user['id'],
					"nik" => $user['nik'],
					"firstname" => $user['firstname'],
					"lastname" => $user['lastname'],
					"email" => $user['email']
				)
			];

			$jwt = JWT::encode($payload, $key);
			return $this->respond(['token' => $jwt], 200);
		}

		return $this->respond(['message' => 'Invalid login details'], 401);
    }
    
    public function register()
	{
		helper('form');
		$data = [];
		$post = $this->request->getVar();

		if($this->request->getMethod() != 'post')
			return $this->fail('Only Post Request is Allowed');
		
		$rules = [
			'nik' => 'required|min_length[3]|max_length[20]|is_unique[users.nik]',
			'firstname' => 'required|min_length[3]|max_length[20]',
			'email' => 'required|valid_email|is_unique[users.email]',
			'password' => 'required|min_length[8]',
			'password_confirm' => 'matches[password]'
		];

		if(!isset($post['lastname'])) {
			$post['lastname'] = '';
		}

		if(!$this->validate($rules)) {
			return $this->fail($this->validator->getErrors());
		} else {
			$data = [
				'nik' => $post['nik'],
				'password' => md5($post['password']),
				'firstname' => $post['firstname'],
				'lastname' => $post['lastname'],
				'email' => $post['email'],
				'scope' => 'app',
				'created_at' => date('Y-m-d H:i:s')
			];

			$user_id = $this->model->insert($data);
			$data['id'] = $user_id;
			unset($data['password']);

			return $this->respondCreated($data);
		}
	}

	public function show($id = null)
	{
		$data = $this->model->find($id);

		if($data) {
			return $this->respond($data);
		} else {
			return $this->failNotFound('Item Not Found');
		}
	}

	public function update($id = null)
	{
		helper(['form']);
		$input = $this->request->getRawInput();

		if($this->request->getMethod() != 'put')
			return $this->fail('Only Put Request is Allowed');
		
		$rules = [
			'nik' => 'required',
			'firstname' => 'required|min_length[3]|max_length[20]',
			'email' => 'required|valid_email|is_unique[users.email]',
			'password' => 'required|min_length[8]',
			'password_confirm' => 'matches[password]'
		];
		
		/* $nik = $input['nik'];		
		$email = $input['email'];
		$db = db_connect();
        $builder = $db->table('users');
        $where = ['nik' => $nik];
        $where = ['id !=' => $id];
        $builder->where($where);
		$checkNik = $builder->get()->getRowArray();
		
		if($checkNik != '') {
			return $this->fail('NIK is already registered find another one');
		}

		$builder = $db->table('users');
        $where = ['email' => $email];
        $where = ['id !=' => $id];
		$builder->where($where);
		$checkMail = $builder->get()->getRowArray();
		if(isset($checkMail)) {
			return $this->fail('email is already registered find another one');
		} */

		if(!isset($post['lastname'])) {
			$post['lastname'] = '';
		}

		if(!$this->validate($rules)) {
			return $this->fail($this->validator->getErrors());
		} else {

			$data = [
				'id' => $id,
				'nik' => $input['nik'],
				'firstname' => $input['firstname'],
				'lastname' => $input['lastname'],
				'email' => $input['email'],
				'scope' => 'app',
				'password' => $input['password'],
				'updated_at' => date('Y-m-d H:i:s')
			];
			$this->model->save($data);
			unset($data['password']);

			return $this->respond($data);
		}
	}

	public function delete($id = null)
	{
		$data = $this->model->find($id);
		
		if($data) {
			$this->model->delete($id);
			return $this->respondDeleted($data);
		} else {
			return $this->failNotFound('Item Not Found');
		}
	}
}
