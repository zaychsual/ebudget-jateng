<?php namespace App\Controllers\Api;

use App\Controllers\BaseController;
use CodeIgniter\RESTful\ResourceController;
use \CodeIgniter\API\ResponseTrait;
use App\Models\M_Blog;

class Blogs extends BaseController
{
	use ResponseTrait;
	protected $format = 'json';

	public function __construct() {
 
        $this->model = new M_Blog;
        
    }

	public function index()
	{
		$posts = $this->model->findAll();

		return $this->respond($posts);
	}	

	public function create()
	{
		helper(['form']);

		$rules = [
			'title' => 'required|min_length[6]',
			'description' => 'required'
		];

		if(!$this->validate($rules)) {
			return $this->fail($this->validator->getErrors());
		} else {
			$data = [
				'title' => $this->request->getVar('title'),
				'description' => $this->request->getVar('description'),
				'status' => 1,
				'created_at' => date('Y-m-d H:i:s')
			];
			$post_id = $this->model->insert($data);
			$data['post_id'] = $post_id;

			return $this->respondCreated($data);
		}
	}

	public function show($id = null)
	{
		$data = $this->model->find($id);

		if($data) {
			return $this->respond($data);
		} else {
			return $this->failNotFound('Item Not Found');
		}
	}

	public function update($id = null)
	{
		helper(['form']);

		$rules = [
			'title' => 'required|min_length[6]',
			'description' => 'required'
		];

		if(!$this->validate($rules)) {
			return $this->fail($this->validator->getErrors());
		} else {
			$input = $this->request->getRawInput();

			$data = [
				'id' => $id,
				'title' => $input['title'],
				'description' => $input['description'],
				'updated_at' => date('Y-m-d H:i:s')
			];

			$this->model->save($data);

			return $this->respond($data);
		}
	}

	public function delete($id = null)
	{
		$data = $this->model->find($id);
		
		if($data) {
			$this->model->delete($id);
			return $this->respondDeleted($data);
		} else {
			return $this->failNotFound('Item Not Found');
		}
	}
}
