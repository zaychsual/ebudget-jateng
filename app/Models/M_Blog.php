<?php namespace App\Models;

use CodeIgniter\Model;

class M_Blog extends Model
{
	protected $table = 'blog';
	protected $primaryKey = 'id';
	protected $allowedFields = [
		'title', 
		'description',
		'status',
		'created_by',
		'updated_by',
		'created_at',
		'updated_at'
	];
}