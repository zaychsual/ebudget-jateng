<?php namespace App\Models;

use CodeIgniter\Model;

class M_user extends Model{
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'nik',
        'password',
        'firstname',
        'lastname',
        'email',
        'email_verified',
        'scope',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'status',
        'level_user'
    ];

    /* protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];


    protected function beforeInsert(array $data){
        $data = $this->passwordHash($data);
        return $data;
    }

    protected function beforeUpdate(array $data){
        $data = $this->passwordHash($data);
        return $data;
    } */
}